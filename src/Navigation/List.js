import React, { Component } from 'react'
import { Text, View, Button } from 'react-native'

export default class List extends Component {
    static navigationOptions = {
        title: 'List Screen',
    };
    render() {
        return (
            <View>
                <Text>Ini List</Text>
                <Button onPress={() => this.props.navigation.navigate('Home')} title='Home' />
            </View>
        )
    }
}
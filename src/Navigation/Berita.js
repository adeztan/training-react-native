import React, { Component } from 'react'
import { Text, View, Button, Image } from 'react-native'

export default class Berita extends Component {
    static navigationOptions = {
        title: 'Berita',
    };
    render() {
        return (
            <View>
                <View>
                    <Text style={{ padding: 5, fontSize: 10 }}>
                        HOME » MAKANAN » MAKANAN TERKINI
              </Text>
                </View>
                <Image source={require('../../dummy/food-banner.jpg')} style={{ height: 170, width: '100%', borderRadius: 7 }} />
                <View style={{ padding: 4, borderBottomColor: '#E8E9ED', borderBottomWidth: 0 }}></View>
                <View style={{ padding: 10, flexDirection: 'row' }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'blue', width: 20 }}>
                        <Text style={{ fontSize: 15, color: '#FFFF' }}>
                            Makanan Enak
                </Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'green' }}>
                        <Text style={{ fontSize: 15, color: '#FFFF' }}>
                            Tongseng Sapi
                </Text>
                    </View>
                </View>
                <View style={{ padding: 5, borderBottomColor: '#E8E9ED', borderBottomWidth: 1 }}></View>
                <View style={{ padding: 2 }}>
                    <Text>
                        <Text style={{ fontWeight: 'bold', color: 'red' }}>
                            Lorem Ipsum
                </Text> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </Text>
                    <View style={{ padding: 4, borderBottomColor: '#E8E9ED', borderBottomWidth: 0 }}></View>
                    <Text>
                        <Text style={{ fontWeight: 'bold', color: 'blue' }}>
                            It is a long established
                </Text>  fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
              </Text>
                </View>
            </View>
        )
    }
}
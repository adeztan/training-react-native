import React from 'react';
import { Text, View, Button, Image } from 'react-native'
import { createDrawerNavigator } from 'react-navigation-drawer'
import { createMaterialTopTabNavigator, createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';


import Home from "./Navigation/Home";
import List from "./Navigation/List";
import Berita from "./Navigation/Berita";

// export const Drawer = createDrawerNavigator({
//     Home: Home,
//     List: List
// })

const HomeTab = createStackNavigator(
    {
        Home: Home ,
      List: List ,
      Berita: Berita ,
    },
    {
      defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: '#0091EA',
        },
        headerTintColor: '#FFFFFF',
        title: 'Home Tab',
       
      },
    }
  );

  const BeritaTab = createStackNavigator(
    {
        Berita: Berita ,
        Home: Home ,
      List: List ,
    },
    {
      defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: '#0091EA',
        },
        headerTintColor: '#FFFFFF',
        title: 'Home Tab',
       
      },
    }
  );

  const ListTab = createStackNavigator(
    {
        List: List ,
        Home: Home ,
      Berita: Berita ,
    },
    {
      defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: '#0091EA',
        },
        headerTintColor: '#FFFFFF',
        title: 'List Tab',
      },
    }
  );  

export const TabNavigation = createBottomTabNavigator({
    Home: HomeTab,
    List: ListTab,
    Berita: BeritaTab,
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                if (routeName === 'Home') {
                    return (
                        <Image
                            source={require('../assets/home.png')}
                            style={{ width: 30, height: 30, }} />
                    );
                } else if (routeName === 'List') {
                    return (
                        <Image
                            source={require('../assets/go-more.png')}
                            style={{ width: 30, height: 30, }} />
                    );
                } else {
                    return (
                        <Image
                            source={require('../assets/account.png')}
                            style={{ width: 20, height: 20 }} />
                    );
                }
            },
        }),
        tabBarOptions: {
            activeTintColor: '#eb4034',
            inactiveTintColor: '#263238',
        },
    })
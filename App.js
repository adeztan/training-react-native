import React from 'react';
import {createAppContainer} from 'react-navigation'
import {Drawer, TabNavigation} from "./src/router";
import {StyleSheet, View} from "react-native";
import Gesture from './src/Gesture';

// const NavPage = createAppContainer(Drawer);
const NavPage = createAppContainer(TabNavigation);

export default class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <NavPage style={styles.tabBarOptions} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5fcff'
    }, 
    tabBarOptions: {
        fontSize: 25,
        width: 100,
        backgroundColor: 'blue',
      }
});